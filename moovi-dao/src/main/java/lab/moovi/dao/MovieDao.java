package lab.moovi.dao;

import lab.moovi.model.Cinema;
import lab.moovi.model.Director;
import lab.moovi.model.Movie;

import java.util.Set;

public interface MovieDao {

    Set<Movie> findAll();

    Movie findById(int id);

    Set<Movie> findByDirector(Director d);

    Set<Movie> findByCinema(Cinema c);

    Movie add(Movie m);

}
