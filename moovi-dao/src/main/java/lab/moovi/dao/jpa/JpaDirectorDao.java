package lab.moovi.dao.jpa;

import lab.moovi.dao.DirectorDao;
import lab.moovi.model.Director;
import lab.moovi.model.Movie;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JpaDirectorDao implements DirectorDao {

    @PersistenceContext(name="moviUnit")
    EntityManager em;

    @Override
    public Set<Director> findAll() {
        List<Director> directors =  em.createQuery("from Director").getResultList();
        return new HashSet<>(directors);
    }

    @Override
    public Director findById(int id) {
        return em.find(Director.class, id);
    }

    @Override
    public Director add(Director d) {
        em.persist(d);
        return d;
    }
}
