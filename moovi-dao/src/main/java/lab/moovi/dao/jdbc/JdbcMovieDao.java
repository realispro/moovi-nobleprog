package lab.moovi.dao.jdbc;

import lab.moovi.dao.MovieDao;
import lab.moovi.model.Cinema;
import lab.moovi.model.Director;
import lab.moovi.model.Movie;

import javax.enterprise.inject.Vetoed;
import java.util.Set;

@Vetoed
public class JdbcMovieDao implements MovieDao {
    @Override
    public Set<Movie> findAll() {
        return null;
    }

    @Override
    public Movie findById(int id) {
        return null;
    }

    @Override
    public Set<Movie> findByDirector(Director d) {
        return null;
    }

    @Override
    public Set<Movie> findByCinema(Cinema c) {
        return null;
    }

    @Override
    public Movie add(Movie m) {
        return null;
    }
}
