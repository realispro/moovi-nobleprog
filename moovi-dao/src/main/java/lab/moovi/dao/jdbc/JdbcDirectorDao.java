package lab.moovi.dao.jdbc;

import lab.moovi.dao.DirectorDao;
import lab.moovi.model.Director;

import javax.enterprise.inject.Vetoed;
import java.util.Set;

@Vetoed
public class JdbcDirectorDao implements DirectorDao {
    @Override
    public Set<Director> findAll() {
        return null;
    }

    @Override
    public Director findById(int id) {
        return null;
    }

    @Override
    public Director add(Director d) {
        return null;
    }
}
