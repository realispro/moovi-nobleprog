package lab.moovi.dao.jdbc;

import lab.moovi.dao.CinemaDao;
import lab.moovi.model.Cinema;
import lab.moovi.model.Movie;

import javax.annotation.Resource;
import javax.enterprise.inject.Vetoed;
import javax.sql.DataSource;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;

@Vetoed
public class JdbcCinemaDao implements CinemaDao {


    @Resource(lookup = "java:jboss/datasources/MooviDS")
    private DataSource dataSource;

    @Override
    public Set<Cinema> findAll() {
        try(Connection c = dataSource.getConnection()){
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "select id, logo, name from CINEMA");
            Set<Cinema> cinemas = new HashSet<>();
            while(rs.next()){
                cinemas.add(map(rs));
            }
            return cinemas;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Cinema findById(int id) {
        try(Connection c = dataSource.getConnection()){
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "select id, logo, name from CINEMA where id=" + id);
            if(rs.next()){
                return map(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        try(Connection c = dataSource.getConnection()){
            PreparedStatement stmt = c.prepareStatement(
                    "select id, logo, name from CINEMA c JOIN MOVIE_CINEMA mc where c.id=mc.cinema_id and mc.movie_id=?");
            stmt.setInt(1, m.getId());
            ResultSet rs = stmt.executeQuery();

            Set<Cinema> cinemas = new HashSet<>();
            while(rs.next()){
                cinemas.add(map(rs));
            }
            return cinemas;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Cinema map(ResultSet rs) throws SQLException {
        Cinema c = new Cinema();
        c.setId(rs.getInt("id"));
        c.setLogo(rs.getString("logo"));
        c.setName(rs.getString("name"));
        return c;
    }
}