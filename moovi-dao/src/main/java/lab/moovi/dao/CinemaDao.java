package lab.moovi.dao;

import lab.moovi.model.Cinema;
import lab.moovi.model.Movie;

import java.util.Set;

public interface CinemaDao {

    Set<Cinema> findAll();

    Cinema findById(int id);

    Set<Cinema> findByMovie(Movie m);

}
