package lab.moovi.dao;


import lab.moovi.dao.mem.MemMovieDao;
import lab.moovi.model.Movie;

import java.util.Set;

public class DaoTest {

    public static void main(String[] args) {

        MovieDao movieDao = new MemMovieDao();
        Set<Movie> movies = movieDao.findAll();

        for( Movie m : movies ){
            System.out.println(m);
        }
    }

}
