package lab.moovi.service.impl;

import lab.moovi.model.Cinema;
import lab.moovi.service.MoviService;

import java.util.Set;

public class ServiceTest {

    public static void main(String[] args) {

        MoviService service = new MoviServiceImpl();
        Set<Cinema> cinemas = service.getAllCinemas();

        for( Cinema c : cinemas){
            System.out.println(c);
        }

    }
}
