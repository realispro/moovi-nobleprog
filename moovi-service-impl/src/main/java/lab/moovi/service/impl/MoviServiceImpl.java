package lab.moovi.service.impl;

import lab.moovi.dao.CinemaDao;
import lab.moovi.dao.DirectorDao;
import lab.moovi.dao.MovieDao;
import lab.moovi.model.Cinema;
import lab.moovi.model.Director;
import lab.moovi.model.Movie;
import lab.moovi.service.MoviService;

import javax.inject.Inject;
import java.util.Set;
import java.util.logging.Logger;



public class MoviServiceImpl implements MoviService {

    Logger logger = Logger.getLogger(MoviService.class.getName());

    @Inject
    private DirectorDao directorDao;

    @Inject
    private CinemaDao cinemaDao;

    @Inject
    private MovieDao movieDao;


    //@Interceptors(MaintenanceModeInterceptor.class)
    public Set<Movie> getAllMovies() {
        logger.info("searching all movies...");
        return movieDao.findAll();
    }

    public Set<Movie> getMoviesByDirector(Director d) {
        logger.info("serching movies by diretor " + d.getId());
        return movieDao.findByDirector(d);
    }

    public Set<Movie> getMoviesInCinema(Cinema c) {
        logger.info("searching movies played in cinema " + c.getId());
        return movieDao.findByCinema(c);
    }

    public Movie getMovieById(int id) {
        logger.info("searching movie by id " + id);
        return movieDao.findById(id);
    }

    public Set<Cinema> getAllCinemas() {
        logger.info("searching all cinemas");
        return cinemaDao.findAll();
    }

    public Set<Cinema> getCinemasByMovie(Movie m) {
        logger.info("searching cinemas by movie " + m.getId());
        return cinemaDao.findByMovie(m);
    }

    public Cinema getCinemaById(int id) {
        logger.info("searching cinema by id " + id);
        return cinemaDao.findById(id);
    }

    public Set<Director> getAllDirectors() {
        logger.info("searching all directors");
        return directorDao.findAll();
    }

    public Director getDirectorById(int id) {
        logger.info("searching director by id " + id);
        return directorDao.findById(id);
    }

    @Override
    public Movie addMovie(Movie m) {
        logger.info("about to add movie " + m );
        return movieDao.add(m);
    }

    @Override
    public Director addDirector(Director d) {
        logger.info("about to add director " + d);
        return directorDao.add(d);
    }

}
