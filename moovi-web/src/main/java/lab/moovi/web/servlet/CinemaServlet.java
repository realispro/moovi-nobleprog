package lab.moovi.web.servlet;

import lab.moovi.model.Cinema;
import lab.moovi.model.Movie;

import lab.moovi.service.MoviService;
import lab.moovi.service.impl.MoviServiceImpl;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet("/cinemas")
public class CinemaServlet extends HttpServlet {

    @Inject
    MoviService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String movie = req.getParameter("movie");

        Set<Cinema> cinemas;
        String title;

        if(movie!=null){
            int movieId = Integer.parseInt(movie);
            Movie m = service.getMovieById(movieId);
            title = "List of cinemas playing " + m.getTitle();
            cinemas = service.getCinemasByMovie(m);
        } else {
            title = "List of cinemas";
            cinemas = service.getAllCinemas();
        }

        req.setAttribute("title", title);
        req.setAttribute("cinemas", cinemas);
        req.getRequestDispatcher("/WEB-INF/jsp/cinemas.jsp").forward(req, resp);

    }
}
