package lab.moovi.web.servlet;

import lab.moovi.model.Cinema;
import lab.moovi.model.Movie;
import lab.moovi.service.MoviService;
import lab.moovi.service.impl.MoviServiceImpl;
import org.apache.commons.lang3.math.NumberUtils;


import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebServlet("/movies")
public class MovieServlet extends HttpServlet {

    @Inject
    MoviService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // /movies?cinema=999&param2=abc
        String cinema = req.getParameter("cinema");

        Set<Movie> movies;
        String title;
        if(cinema!=null){

            boolean parsable = NumberUtils.isParsable(cinema);
            if(parsable) {
                int cinemaId = Integer.parseInt(cinema);

                Cinema c = service.getCinemaById(cinemaId);
                if (c != null) {
                    title = "List of cinemas in " + c.getName();
                    movies = service.getMoviesInCinema(c);
                } else {
                    title = "Missing cinema of id " + cinemaId;
                    movies = new HashSet();
                }
            } else {
                title = "Non parseable id provided: " + cinema;
                movies = new HashSet<>();
            }
        } else {
            movies = service.getAllMovies();
            title = "List of movies";
        }
        req.setAttribute("movies", movies);
        req.setAttribute("title", title);

        req.getRequestDispatcher("/WEB-INF/jsp/movies.jsp").forward(req, resp);
    }
}
