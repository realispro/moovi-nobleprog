package lab.moovi.web.rest;

import lab.moovi.model.Cinema;
import lab.moovi.service.MoviService;
import lab.moovi.web.rest.dto.CinemaDTO;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.Set;

//  http://localhost:8080/moovi/webapi/cinemas
@Path("/cinemas")
public class CinemasREST {

    @Inject
    MoviService service;

    @GET
    public Set<Cinema> getCinemas(){
        return service.getAllCinemas();
    }

    @GET
    @Path("/{xyz}")
    public Cinema getCinema(@PathParam("xyz") int id){
        return service.getCinemaById(id);
    }

    @POST
    public Response addCinema(Cinema c){
        System.out.println("cinema from json: " + c);

        return Response
                .status(Response.Status.CREATED).entity("created").build();

    }


}
