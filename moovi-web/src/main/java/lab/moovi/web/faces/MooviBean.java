package lab.moovi.web.faces;

import lab.moovi.model.Cinema;
import lab.moovi.model.Movie;
import lab.moovi.service.MoviService;
import lab.moovi.service.impl.MoviServiceImpl;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.Map;
import java.util.Set;

@ManagedBean(name="mooviBean")
public class MooviBean {

    @Inject
    MoviService service;

    public Set<Cinema> getAllCinemas(){
        return service.getAllCinemas();
    }

    public Set<Movie> getMovies(){

        String cinema = getParameter("cinema");

        if(cinema!=null){
            int cinemaId = Integer.parseInt(cinema);
            Cinema c = service.getCinemaById(cinemaId);
            return service.getMoviesInCinema(c);
        }

        return service.getAllMovies();
    }

    private String getParameter(String name) {
        Map<String, String> params =
        FacesContext.
                getCurrentInstance().
                getExternalContext().
                getRequestParameterMap();
        return params.get(name);
    }

}
