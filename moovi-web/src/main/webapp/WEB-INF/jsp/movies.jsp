<%@ page import="lab.moovi.model.Cinema" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"/>

        <table>
            <tr>
                <th>Poster</th>
                <th>Title</th>
                <th>Rating</th>
                <th>Director</th>
                <th>Cinemas</th>
            </tr>
            <c:forEach items="${requestScope.movies}" var="movie">
                <tr>
                    <td><img src="${movie.poster}"></td>
                    <td><c:out value="${movie.title}"/></td>
                    <td>${movie.rating}</td>
                    <td>${movie.director.lastName} ${movie.director.firstName}</td>
                    <td><a href='<c:url value="/cinemas?movie=${movie.id}"/>'>show</a></td>
                </tr>
            </c:forEach>

        </table>

<jsp:include page="footer.jsp"/>
