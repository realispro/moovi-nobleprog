<%@ page import="lab.moovi.model.Cinema" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"/>

        <table>
            <tr>
                <th>Logo</th>
                <th>Name</th>
                <th>Movies</th>
            </tr>
            <c:forEach items="${requestScope.cinemas}" var="cinema">
                <tr>
                    <td><img src="${cinema.logo}"></td>
                    <td>${cinema.name}</td>
                    <td><a href='<c:url value="/movies?cinema=${cinema.id}"/>'>show</a></td>
                </tr>
            </c:forEach>

        </table>

<jsp:include page="footer.jsp"/>
