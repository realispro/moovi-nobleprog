<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Moovi</title>
    <link rel="stylesheet" href='<c:url value="/css/style.css"/>'/>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>
</head>
<body>
    <header>
        <h1>Moovi</h1>
        <h2>${requestScope.title}</h2>
    </header>


    <section>
